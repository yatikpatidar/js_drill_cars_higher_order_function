// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an // console.log(`car 33 is ${carById.} ${carById[1]} ${carById[2]} ${carById[3]}`)array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const {inventory} = require('../app')

const BMWAndAudiCars = require('../problem6')

console.log(BMWAndAudiCars.BMWAndAudi(inventory));
// console.log(BMWAndAudiCars.BMWAndAudi(inventory).length);

