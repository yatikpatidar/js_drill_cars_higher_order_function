module.exports = {
    lastCarDetail:function(inventory){
        let n = inventory.length;
        
        return(`Last car is a ${inventory[n-1]['car_make']} ${inventory[n-1]['car_model']}`)
     
    }
}