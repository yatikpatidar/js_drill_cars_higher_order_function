// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
const carYearList = require('./problem4')
module.exports = {
    olderCars : function(inventory){
        let count = 0 ;
        
        const yearList = inventory.map((inv)=>{
            return inv['car_year']
        })
        for(let i =0 ; i<yearList.length ; i++){
            if (yearList[i] < 2000){
                count += 1 ;
            }
        }
        return count ;



    }
}