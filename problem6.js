// problem 6

module.exports = {
    BMWAndAudi : function(inventory){
        
        const carId = inventory.filter((inv)=>{
            if(inv['car_make'] === 'BMW' || inv['car_make'] === 'Audi'){
                return inv
            }
        })

        
        return JSON.stringify(carId)
        
    }
}